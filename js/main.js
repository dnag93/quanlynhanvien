let employeeList = [];
let timer;

let addEmployee = () => {
  const lastName = document.getElementById("ho").value;
  const firstName = document.getElementById("ten").value;
  const id = document.getElementById("msnv").value;
  const dob = document.getElementById("datepicker").value;
  const position = document.getElementById("chucvu").value;

  let isValid = checkInput(lastName, firstName, id);
  let addMessage = document.getElementById("addError");

  if (isValid) {
    const newEmployee = new Employee(lastName, firstName, id, dob, position);
    employeeList.push(newEmployee);
    addMessage.innerHTML = "Đã thêm nhân viên";
    resetForm();
    //lưu employeeList vào local storage, render table

    localStorage.setItem("listNV", JSON.stringify(employeeList));
    createTable(employeeList);
  } else {
    addMessage.innerHTML = "Thông tin nhân viên nhập sai";
  }
};

let getDataFromLocal = () => {
  const employeeListStr = localStorage.getItem("listNV");
  if (employeeListStr) {
    //mảng lấy lên từ local, các đối tượng trong nó không có phương thức
    employeeListFromLocal = JSON.parse(employeeListStr);
    //để sử dụng được, cần chuyển sang mảng mới
    employeeListFromLocal.forEach(employee => {
      //đối tương mới tạo ra từ đối tượng trên, nhưng có phương thức calcSalary
      const newEmployee = new Employee(
        employee.lastName,
        employee.firstName,
        employee.id,
        employee.dob,
        employee.position
      );
      employeeList.push(newEmployee);
    });
    createTable(employeeList);
  }
};

let createTable = list => {
  let contentTable = "";
  list.forEach((employee, i) => {
    contentTable += `
    <tr>
        <td>${i + 1}</td>
        <td>${employee.id}</td>
        <td>${employee.lastName + " " + employee.firstName}</td>
        <td>${employee.dob}</td>
        <td>${employee.position}</td>
        <td>${employee.calcSalary()}</td>
        <td>
           <button
              class="btn btn-danger"
              onclick="removeEmployee('${employee.id}')"
           >
               Xoá
           </button>
           <button class="btn btn-info"
               onclick="getUpdateEmployee('${employee.id}')"
           >Cập nhật</button>
        </td>
    </tr>`;
  });

  document.getElementById("tbodyEmployee").innerHTML = contentTable;
};

// ============"FEATURES"====================
// break, return và countinue không có tác dụng với forEach
// for In rất dễ nhầm lẫn ngữ nghĩa là value bên trong
// let findIndex = id => {
//   for (let index in employeeList) {
//     if (employeeList[index].id === id) {
//       return index;
//     }
//   }
//   return -1;
// };

let findEmployeeIndex = id =>
  employeeList.findIndex(employee => employee.id === id);

let resetForm = () => document.getElementById("btnReset").click();

let removeEmployee = id => {
  //input: mã nhân viên
  /*
        1.lấy input từ params
        2.Tìm index :for() mang[i].id === id => i;
        3.Xoá: employeeList.splice(index,n)
        [1,2,3].splice(1,1) => [1,3]
        [1,2,3].splice(1,2) => [1]
    */
  //output: employeeList
  const index = findEmployeeIndex(id);
  if (index >= 0) {
    employeeList.splice(index, 1);
    localStorage.setItem("listNV", JSON.stringify(employeeList));
    createTable(employeeList);
  }
};
// ======================================
//input: mã nhân viên
/*
  1.lấy input từ params
  2.tìm vị trí
  3.show thông tin lên form
  ==> get info của nhân Viên được update thông tin lên form
    document.getElementById('ho').value = employee.lastName
  ==> thực hiện update
  4.ẩn button thêm, hiện button lưu
*/
//output: đối tượng nhân viên có id === id & show thông tin lên form

let getUpdateEmployee = id => {
  const index = findEmployeeIndex(id);
  if (index >= 0) {
    document.getElementById("ho").value = employeeList[index].lastName;
    document.getElementById("ten").value = employeeList[index].firstName;
    document.getElementById("msnv").value = employeeList[index].id;
    document.getElementById("datepicker").value = employeeList[index].dob;
    document.getElementById("chucvu").value = employeeList[index].position;

    document.getElementById("btnUpdate").style.display = "block";
    document.getElementById("btnAdd").style.display = "none";
    document.getElementById("msnv").setAttribute("readonly", true);
  }
};

let updateEmployee = () => {
  const lastName = document.getElementById("ho").value;
  const firstName = document.getElementById("ten").value;
  const id = document.getElementById("msnv").value;
  const dob = document.getElementById("datepicker").value;
  const position = document.getElementById("chucvu").value;
  //Employee là 1 lớp đối tượng (prototype)
  // newEmployee là 1 đối tượng Employee (instance)
  // được tạo ra từ lớp Employee

  // Tạo 1 object Employee mới rồi đè vào object cũ trong list
  const updatedEmployee = new Employee(lastName, firstName, id, dob, position);

  const index = findEmployeeIndex(updatedEmployee.id);
  employeeList[index] = updatedEmployee;
  localStorage.setItem("listNV", JSON.stringify(employeeList));

  document.getElementById("btnUpdate").style.display = "none";
  document.getElementById("btnAdd").style.display = "block";
  document.getElementById("msnv").removeAttribute("readonly");

  resetForm();
  // Kỹ thuật ??? > click ngầm sau khi user click trên giao diện

  createTable(employeeList);
};
// ======================================
let searchEmployee = () => {
  const searchList = [];

  // search real - time astype
  // Dùng ajax - xử lý bất đồng bộ
  // Về lý thuyết thì xem lại hình
  // ajax sẽ render chỉ khi các function đồng bộ chạy xong hết

  // if (timer) {
  //   clearTimeout(timer);
  // }
  // kỹ thuật đè lại timer cũ, viết gọn lại
  timer && clearTimeout(timer);
  // keyUp sau sẽ đè timer của key trước
  // tránh việc DB load render liên tục
  // chỉ render khi user dừng input 1 khoảng timer

  // hander hay para của hàm setTimer ở đây dùng 1 hàm ẩn danh
  // hàm chỉ xài 1 lần, ko cần tên
  timer = setTimeout(() => {
    const keyword = document
      .getElementById("txtSearch")
      .value.toLowerCase()
      .trim();
    //  đưa về lowercase hết tránh tên viết hoa
    employeeList.forEach(employee => {
      let fullName = employee.lastName + " " + employee.firstName;
      fullName = fullName.toLowerCase();

      (employee.id === keyword || fullName.indexOf(keyword) !== -1) &&
        searchList.push(employee);
    });
    createTable(searchList);
  }, 400);
};
// ======================================
// 3 hàm check riêng,
// có thể refactor lại thành hàm lớn nhưng dễ bug
// Sử dụng Short-circuit evaluation với toán tử && , ||

let checkRequired = (value, idSpan) => {
  // check rỗng
  const spanMessage = document.getElementById(idSpan);
  if (value) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = "(*) Dữ liệu bắt buộc nhập!";
  return false;
};

let checkLength = (value, idSpan, min, max) => {
  // check số kí tự
  const spanMessage = document.getElementById(idSpan);
  if (value.length >= min && value.length <= max) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `(*) Dữ liệu nhập từ ${min} đến ${max} kí tự`;
  return false;
};

let checkText = (value, idSpan) => {
  //Regex
  // check kí tự hợp lệ
  // const pattern = /^[a-zA-Z ]+$/g;
  const pattern = new RegExp(
    "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
      "ẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
      "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$"
  );
  const spanMessage = document.getElementById(idSpan);
  if (pattern.test(value)) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `(*) Dữ liệu nhập phải là chữ.`;
  return false;
};
// ======================================
// ==== 1 hàm lớn bao lại cho dễ quản lí
// Sử dụng phép &=
// Xem thêm về Assignment Operator

let checkInput = (lastName, firstName, id) => {
  let isValid = true;
  isValid &=
    checkRequired(firstName, "firstNameError") &&
    checkText(firstName, "firstNameError");

  isValid &=
    checkRequired(lastName, "lastNameError") &&
    checkText(lastName, "lastNameError");

  isValid &= checkRequired(id, "idError") && checkLength(id, "idError", 4, 6);
  return isValid;
};
getDataFromLocal();
