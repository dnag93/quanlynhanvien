//function constructor => khởi tạo 1 lớp đối tượng Employee
class Employee {
  constructor(lastName, firstName, id, dob, position) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.id = id;
    this.dob = dob;
    this.position = position;
    this.calcSalary = () => {
      switch (position) {
        case "Sếp":
          return 90000;
        case "Trưởng phòng":
          return 10000;
        default:
          return 2000;
      }
    };
  }
}

